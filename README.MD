# 简介
Jweb-boot 是基于 [JFinal](http://jfinal.com/) + [Jboot](http://jboot.io/) +undertow进行开发增强而来的微服务极速开发基础框架，无它！  

## 特点
在[Jboot](http://jboot.io/) 的基础上开发，所有继承了[Jboot](http://jboot.io/) + [JFinal](http://jfinal.com/) 的全部特点，并在此基础上进行增强。

###  [Jboot](http://jboot.io/) 特点

1. 基于 [JFinal](http://jfinal.com/) 的 MVC + ORM 快速开发。
1. 基于 ShardingSphere + Seata 分布式事务 和 分库分表。
1. 基于 Dubbo 或 Motan 的 RPC 实现
1. 基于 Sentinel 的分布式限流和降级
1. 基于 Apollo 和 Nacos 的分布式配置中心
1. 基于 EhCache 和 Redis 的分布式二级缓存

### Jweb-boot特点

1. 提供jweb-discovery模块，支撑网关服务代理，提供各服务间直接发现与调用
1. 提供jweb-security安全框架，借鉴于shiro，简化用户认证权限授权系统开发。
1. 增强 [Jboot](http://jboot.io/) 网关功能，适配jweb-discovery服务发现功能
1. 增强Nacos分布式配置功能
1. 增强jboot资源监控，资源查找功能



## 附录
1. [Jweb极速开发平台](https://www.gitee.com/imlzw/jweb-adai)： [jweb-adai](https://www.gitee.com/imlzw/jweb-adai) 
2. [Jweb极速开发官网](http://jweb.cc)： [jweb.cc](http://jweb.cc)
