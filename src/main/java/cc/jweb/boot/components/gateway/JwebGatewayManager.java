/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.jweb.boot.components.gateway;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.Ret;
import io.jboot.app.config.JbootConfigUtil;
import io.jboot.components.gateway.JbootGatewayConfig;
import io.jboot.components.http.JbootHttpRequest;
import io.jboot.utils.HttpUtil;
import io.jboot.utils.NamedThreadFactory;
import io.jboot.utils.StrUtil;
import io.jboot.web.render.JbootJsonRender;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author michael yang (fuhai999@gmail.com)
 * @Date: 2020/3/21
 */
public class JwebGatewayManager {

    private static JwebGatewayManager me = new JwebGatewayManager();
    private ConcurrentHashMap<String, JwebGatewayConfig> configMap;
    private ScheduledThreadPoolExecutor fixedScheduler;
    private long fixedSchedulerInitialDelay = 10;
    private long fixedSchedulerDelay = 10;
    private NoneHealthUrlErrorRender noneHealthUrlErrorRender;

    public static JwebGatewayManager me() {
        return me;
    }

    public boolean isEnableAndConfigOk() {
        return configMap != null && !configMap.isEmpty();
    }

    private JwebGatewayManager() {
        Map<String, JwebGatewayConfig> configMap = JbootConfigUtil.getConfigModels(JwebGatewayConfig.class, "jweb.gateway");
        if (configMap != null && !configMap.isEmpty()) {

            for (Map.Entry<String, JwebGatewayConfig> e : configMap.entrySet()) {
                JwebGatewayConfig config = e.getValue();
                if (config.isConfigOk() && config.isEnable()) {
                    if (StrUtil.isBlank(config.getName())) {
                        config.setName(e.getKey());
                    }
                    registerConfig(config);
                }
            }
        }
    }

    public void init() {
        // 启动定时健康检查
        if (isEnableAndConfigOk()) {
            startHealthCheckIfNecessary();
        }

    }

    /**
     * 开始健康检查
     * 多次执行，只会启动一次
     */
    private void startHealthCheckIfNecessary() {
        if (fixedScheduler == null) {
            synchronized (this) {
                if (fixedScheduler == null) {
                    fixedScheduler = new ScheduledThreadPoolExecutor(1, new NamedThreadFactory("jweb-gateway-health-check"));
                    //每 10s 进行一次健康检查
                    fixedScheduler.scheduleWithFixedDelay(() -> {
                        try {
                            doHealthCheck();
                        } catch (Exception ex) {
                            LogKit.error(ex.toString(), ex);
                        }
                    }, fixedSchedulerInitialDelay, fixedSchedulerDelay, TimeUnit.SECONDS);
                }
            }
        }
    }

    /**
     * 健康检查
     */
    private void doHealthCheck() {
        for (JwebGatewayConfig config : configMap.values()) {
            if (config.isUriHealthCheckEnable() && StrUtil.isNotBlank(config.getUriHealthCheckPath())) {
                String[] uris = config.getUri();
                for (String uri : uris) {
                    String url = uri + config.getUriHealthCheckPath();
                    // 跳过 服务发现格式地址，如： http://{demo}
                    if (url.indexOf("{") >= 0 && url.indexOf("}") >=0 ) {
                        continue;
                    }
                    if (getHttpCode(url) == 200) {
                        config.removeUnHealthUri(uri);
                    } else {
                        config.addUnHealthUri(uri);
                    }
                }
            }
        }
    }

    private int getHttpCode(String url) {
        try {
            JbootHttpRequest req = JbootHttpRequest.create(url);
            req.setReadBody(false);
            return HttpUtil.handle(req).getResponseCode();
        } catch (Exception ex) {
            // do nothing
            return 0;
        }
    }


    public synchronized void registerConfig(JwebGatewayConfig config) {
        if (configMap == null) {
            configMap = new ConcurrentHashMap<>();
        }
        configMap.put(config.getName(), config);
    }


    public JwebGatewayConfig removeConfig(String name) {
        return configMap == null ? null : configMap.remove(name);
    }


    public JwebGatewayConfig getConfig(String name) {
        return configMap == null ? null : configMap.get(name);
    }


    public Map<String, JwebGatewayConfig> getConfigMap() {
        return configMap;
    }

    public ScheduledThreadPoolExecutor getFixedScheduler() {
        return fixedScheduler;
    }

    public long getFixedSchedulerInitialDelay() {
        return fixedSchedulerInitialDelay;
    }

    public void setFixedSchedulerInitialDelay(long fixedSchedulerInitialDelay) {
        this.fixedSchedulerInitialDelay = fixedSchedulerInitialDelay;
    }

    public long getFixedSchedulerDelay() {
        return fixedSchedulerDelay;
    }

    public void setFixedSchedulerDelay(long fixedSchedulerDelay) {
        this.fixedSchedulerDelay = fixedSchedulerDelay;
    }

    /**
     * 匹配可用的网关
     *
     * @param req 请求
     * @return 返回匹配到的网关配置
     */
    public JwebGatewayConfig matchingConfig(HttpServletRequest req) {
        if (configMap != null && !configMap.isEmpty()) {
            Iterator<JwebGatewayConfig> iterator = configMap.values().iterator();
            while (iterator.hasNext()) {
                JwebGatewayConfig config = iterator.next();
                if (config.matches(req)) {
                    return config;
                }
            }
        }
        return null;
    }

    public NoneHealthUrlErrorRender getNoneHealthUrlErrorRender() {
        return noneHealthUrlErrorRender;
    }

    public void setNoneHealthUrlErrorRender(NoneHealthUrlErrorRender noneHealthUrlErrorRender) {
        this.noneHealthUrlErrorRender = noneHealthUrlErrorRender;
    }

    public void renderNoneHealthUrl(JwebGatewayConfig config, HttpServletRequest request, HttpServletResponse response) {
        if (noneHealthUrlErrorRender != null) {
            noneHealthUrlErrorRender.render(config, request, response);
        } else {
            new JbootJsonRender(Ret.fail().set("message", "none health url in gateway.")).setContext(request, response).render();
        }
    }

}
