/*
 * Copyright (c) 2020-2021 imlzw@vip.qq.com jweb.cc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cc.jweb.boot.app.undertow;

import cc.jweb.boot.kit.JwebClassLoaderKit;
import com.jfinal.server.undertow.PropExt;
import com.jfinal.server.undertow.hotswap.ClassLoaderKit;
import io.jboot.app.undertow.JbootUndertowConfig;
import io.undertow.Undertow;
import io.undertow.servlet.api.ErrorPage;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class JwebUndertowConfig extends JbootUndertowConfig {

    static final String WELCOME_PAGES = "undertow.welcomePages";
    static final String ERROR_PAGES = "undertow.errorPages";

    public JwebUndertowConfig(Class<?> jfinalConfigClass) {
        super(jfinalConfigClass);
    }

    public JwebUndertowConfig(String jfinalConfigClass) {
        super(jfinalConfigClass);
        this.resetClassLoaderKit();
    }


    protected ClassLoaderKit getClassLoaderKit() {
        if (classLoaderKit == null) {
            classLoaderKit = new ClassLoaderKit(Undertow.class.getClassLoader(), getHotSwapResolver());
        }
        return classLoaderKit;
    }

    /**
     * 重置classLoadKit对象为JwebClassLoadKit对象
     *
     */
    private void resetClassLoaderKit() {
        classLoaderKit = new JwebClassLoaderKit(Thread.currentThread().getContextClassLoader(), getHotSwapResolver());
        System.out.println("Jweb reset JFinal -> UndertowConfig -> classLoaderKit: " + classLoaderKit);
    }

    public JwebUndertowConfig(Class<?> jfinalConfigClass, String undertowConfig) {
        super(jfinalConfigClass, undertowConfig);
    }

    public JwebUndertowConfig(String jfinalConfigClass, String undertowConfig) {
        super(jfinalConfigClass, undertowConfig);
    }

    /**
     * 重置PropExt对象的classLoader
     */
    public static void resetPropExtClassLoaderKit(){
        Field classLoaderKit = null;
        try {
            classLoaderKit = PropExt.class.getDeclaredField("classLoaderKit");
            classLoaderKit.setAccessible(true);
            try {
                JwebClassLoaderKit jwebClassLoaderKit = new JwebClassLoaderKit(Thread.currentThread().getContextClassLoader(), null);
                classLoaderKit.set(null, jwebClassLoaderKit);
                System.out.println("Jweb reset JFinal -> UndertowConfig -> PropExt -> classLoaderKit: " + jwebClassLoaderKit);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取 welcome pages
     * @return
     */
    public String[] getWelcomePages() {
        String pages = p.get(WELCOME_PAGES, "index.html,index.htm,index");
        return pages.split(",");
    }

    /**
     * 获取错误页面列表
     *
     * @return
     */
    public List<ErrorPage> getErrorPages() {
        List<ErrorPage> errorList  = new ArrayList<>(8);
        String errorPages = p.get("undertow.errorPages");
        if (errorPages != null) {
            String[] errorPageArr = errorPages.split(",");
            if (errorPageArr != null) {
                for (String errorPage : errorPageArr) {
                    if (errorPage == null) {
                        continue;
                    }
                    String[] pageInfo = errorPage.split(":");
                    if (pageInfo == null || pageInfo.length < 2) {
                        continue;
                    }
                    int errorCode = 0;
                    try {
                        errorCode = Integer.parseInt(pageInfo[0]);
                        String page = pageInfo[1];
                        errorList.add(new ErrorPage(page, errorCode));
                    } catch (Exception e) {
                    }
                }
            }
        }
        return errorList;
    }

}
