package cc.jweb.boot.test.index.controller;

import cc.jweb.boot.app.JwebApplication;
import cc.jweb.boot.controller.JwebController;
import io.jboot.web.controller.annotation.RequestMapping;

@RequestMapping(value = "/")
public class IndexController extends JwebController {

    public static void main(String[] args) {
        JwebApplication.main(args);
    }

    public void index() {
        renderText("Hello World Jweb boot!");
    }
}
